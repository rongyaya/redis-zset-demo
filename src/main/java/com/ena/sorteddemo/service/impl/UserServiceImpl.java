package com.ena.sorteddemo.service.impl;

import com.ena.sorteddemo.dao.UserDao;
import com.ena.sorteddemo.entity.User;
import com.ena.sorteddemo.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author zgr
 * @create 2023/8/8 21:55
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public List<User> getList() {
        return userDao.findAll();
    }

    @Override
    public void addUser(User user) {
        userDao.save(user);
    }
}
