package com.ena.sorteddemo.service;

import com.ena.sorteddemo.entity.User;

import java.util.List;

/**
 * @Author zgr
 * @create 2023/8/8 21:55
 */
public interface UserService {

    List<User> getList();

    void addUser(User user);
}
