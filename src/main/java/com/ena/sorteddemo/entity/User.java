package com.ena.sorteddemo.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @Author zgr
 * @create 2023/8/8 21:50
 */
@Data
@Entity
@Table(name = "t_user")
public class User {
    /**
     * 主键生成策略： 自增
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Integer nums;
}
