package com.ena.sorteddemo.dao;

import com.ena.sorteddemo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * @Author zgr
 * @create 2023/8/8 21:54
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer>, Serializable {
}
