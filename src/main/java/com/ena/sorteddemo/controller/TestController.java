package com.ena.sorteddemo.controller;

import com.ena.sorteddemo.utils.RedisUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @Author zgr
 * @create 2023/8/9 09:54
 */
@RestController
public class TestController {

    private final RedisUtil redisUtil;

    private static final String REDIS_PREFIX = "USER:SORTED:KEY";

    public TestController(RedisUtil redisUtil) {
        this.redisUtil = redisUtil;
    }

    /**
     * 从大到小排序
     * @return
     */
    @GetMapping("/sort")
    public Set<String> sort() {
        return redisUtil.zReverseRange(REDIS_PREFIX, 0 , -1);
    }
}
