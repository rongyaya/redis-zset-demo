package com.ena.sorteddemo.config;

import cn.hutool.core.collection.CollUtil;
import com.ena.sorteddemo.entity.User;
import com.ena.sorteddemo.service.UserService;
import com.ena.sorteddemo.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * @Author zgr
 * @create 2023/8/8 21:53
 */
@Component
public class UserInitConfig {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisUtil redisUtil;

    private static final String REDIS_PREFIX = "USER:SORTED:KEY";

    @PostConstruct
    public void init() {
        if (redisUtil.hasKey(REDIS_PREFIX)) {
            redisUtil.del(REDIS_PREFIX);
        }
        List<User> users = userService.getList();
        if (CollUtil.isNotEmpty(users)) {
            for (User user: users) {
                redisUtil.zAdd(REDIS_PREFIX, user.getName(),  user.getNums().doubleValue());
            }
        }
    }
}
