package com.ena.sorteddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SortedDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SortedDemoApplication.class, args);
    }

}
